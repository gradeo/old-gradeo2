import { createSlice } from '@reduxjs/toolkit';

// REDUX
// oldState - начальное состояние
// newState - новое состояние
// reducer - обработчик
// action - действие
// dispatch - создание действия и активация обработчиков
//
//  action  <---(dispatch)-|
//    |                    |
//   \|/                   |
//  reducer <- (oldState)  |
//    |                    |
//   \|/ (newState)        |
//  store                  |
//    |                    |
//   \|/ (update data)     |
//  components ------------|

// Элемент нашего глобального состояния - slice
const drawerSlice = createSlice({
    name: 'drawer', // Название состояния
    initialState: true, // Начальное состояние - true
    reducers: { // Обработчики действий над состоянием.
      // Описываются в формате: action : reducer (действие : обработчик)
      open: state => true, // Действие open меняет состояние на true
      close: state => false, // Действие close меняет состояние на false
      toggle: state => !state // Действие toggle инвертирует состояние нашей боковой панели
    }
})

// Получаем объект действий и обработчик (совмещенный)
const { actions, reducer } = drawerSlice;
// Деструктуризируем действия на набор конкретных действий
const { open, close, toggle } = actions;

export {
    reducer,
    open,
    close,
    toggle
}