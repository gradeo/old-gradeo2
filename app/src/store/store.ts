import { configureStore, combineReducers } from '@reduxjs/toolkit';
import { reducer as drawerReducer } from './slices/drawer.slice';
import { reducer as loaderReducer } from './slices/loader.slice';

// Начальное состояние - читаем его из LocalStorage
const preloadedState = localStorage.getItem('reduxState')
    ? JSON.parse(localStorage.getItem('reduxState') || "{}")
    : {}

// Объединяем обработчики состояния приложения в 1 общий обработчик
const rootReducer = combineReducers({
    drawer: drawerReducer,
    loader: loaderReducer,
})

// Хранилище нашего состояния + глобальный обработчик
export const store = configureStore({
    reducer: rootReducer,
    preloadedState,
});

// Подписываемся на изменения состояния, при этом сохраняем его в LocalStorage
store.subscribe(() => {
    localStorage.setItem('reduxState', JSON.stringify(store.getState()))
})

export type RootState = ReturnType<typeof rootReducer>