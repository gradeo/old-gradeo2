import React, { FC } from 'react';
// import Navbar from '../navbar'
import useThemes from "./theme";
import { ThemeProvider } from "@material-ui/core/styles";
import { KeycloakProvider } from '@react-keycloak/web'
import Layout from './Layout';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider as StateProvider } from 'react-redux';
import { store } from './store/store';
import { ToastContainer } from 'react-toastify';
import keycloak, { keycloakProviderInitConfig } from './shared/api/keycloak';

const App: FC = () => {
  const onKeycloakEvent = (event: any, error: any) => {
    console.log('onKeycloakEvent', event, error)
  }

  const onKeycloakTokens = (tokens: any) => {
    console.log('onKeycloakTokens', tokens)
    sessionStorage.setItem("authentication", tokens.token)
  }

  return (
    <KeycloakProvider
        keycloak={keycloak}
        initConfig={keycloakProviderInitConfig}
        onEvent={onKeycloakEvent}
        onTokens={onKeycloakTokens}
    >
      <StateProvider store={store}>
        <ThemeProvider theme={useThemes}>
          <Router>
            <Layout />
            <ToastContainer position="bottom-right" />
          </Router>
        </ThemeProvider>
      </StateProvider>
    </KeycloakProvider>
  );
}

export default App;