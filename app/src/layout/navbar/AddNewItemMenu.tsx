import React, { FC, useState, MouseEvent } from 'react'
import { Button, Menu, MenuItem } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';

export const AddNewItemMenu: FC = () => {
    const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

    const handleClick = (event: MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (<div>
        <Button 
            variant="contained" 
            color="primary"
            disableElevation
            aria-controls="add-new-menu" 
            aria-haspopup="true" 
            onClick={handleClick}
            startIcon={<AddIcon />}
        >
            Добавить
        </Button>
        <Menu
            id="add-new-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
        >
            <MenuItem onClick={handleClose}>Оценка</MenuItem>
            <MenuItem onClick={handleClose}>Посещаемость</MenuItem>
            <MenuItem onClick={handleClose}>Отчет</MenuItem>
        </Menu>
    </div>)
}