import React from "react";
import { Button, AppBar, Toolbar, IconButton, Chip } from "@material-ui/core";
import Logo from '../../assets/images/logo.svg'
import MenuIcon from '@material-ui/icons/Menu';
import useStyles from "../../layout-styles";
import { AddNewItemMenu } from "./AddNewItemMenu";
import { useDispatch } from "react-redux";
import { toggle as toggleDrawer } from '../../store/slices/drawer.slice';
import { useKeycloak } from "@react-keycloak/web";

export function AppNavBar(): JSX.Element {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [keycloak] = useKeycloak();

    return (
        <AppBar
            position="fixed"
            elevation={0}
            className={classes.appBar}
            color="secondary"
        >
            <Toolbar>
                <IconButton color="inherit"
                    aria-label="open drawer"
                    onClick={() => dispatch(toggleDrawer())}
                    edge="start"
                >
                    <MenuIcon />
                </IconButton>
                <div className={classes.logo}>
                    <img src={Logo} width="100" height="24" alt="Gradeo" />
                    <Chip size="small" style={{ marginLeft: 8 }} label="alpha" />
                </div>
                <div className={classes.createNewButton}>
                    <AddNewItemMenu />
                </div>
                {keycloak && !!keycloak.authenticated && (
                    <Button onClick={() => keycloak.logout()} color="primary">Выход</Button>
                )}

            </Toolbar>
        </AppBar>
    );
}