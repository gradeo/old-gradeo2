import React, { FC } from 'react';
import { Drawer, Toolbar, Divider, List } from '@material-ui/core';
import useStyles from '../../layout-styles';
import { DrawerLink } from './DrawerLink';
import HomeIcon from '@material-ui/icons/Home';
import InfoIcon from '@material-ui/icons/Info';
import SettingsIcon from '@material-ui/icons/Settings';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import DescriptionOutlinedIcon from '@material-ui/icons/DescriptionOutlined';
import SpellcheckOutlinedIcon from '@material-ui/icons/SpellcheckOutlined';
import ImportExportIcon from '@material-ui/icons/ImportExport';
import {DrawerSection} from "./DrawerSection";
import WidgetsOutlinedIcon from '@material-ui/icons/WidgetsOutlined';

interface AppDrawerProps {
    open: boolean
}

export const AppDrawer: FC<AppDrawerProps> = ({ open }) => {
    const classes = useStyles();
    
    return <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={open}
        classes={{
            paper: classes.drawerPaper,
        }}
    >
        <Toolbar />
        <div className={classes.drawerContainer}>
            <List>
                <DrawerLink title="Главная" to="/" icon={<HomeIcon />}/>
                <DrawerSection title="Данные" icon={<WidgetsOutlinedIcon />}>
                    <DrawerLink title="Институты" to="/institutes" />
                    <DrawerLink title="Кафедры" to="/departments" />
                    <DrawerLink title="Группы" to="/groups" />
                    <DrawerLink title="Студенты" to="/students"/>
                    <DrawerLink title="Предметы" to="/subjects" />
                    <DrawerLink title="Занятия" to="/lessons" />
                </DrawerSection>
                <DrawerLink title="Посещаемость" to="/attendance" icon={<AssignmentTurnedInIcon />}/>
                <DrawerLink title="Оценивание" to="/grades" icon={<SpellcheckOutlinedIcon />}/>
                <DrawerLink title="Отчеты" to="/reports" icon={<DescriptionOutlinedIcon />}/>
            </List>
            <Divider />
            <List>
                <DrawerLink title="Импорт/Экспорт" to="/import" icon={<ImportExportIcon />}/>
                <DrawerLink title="Настройки" to="/settings" icon={<SettingsIcon />}/>
                <DrawerLink title="О Gradeo" to="/about" icon={<InfoIcon />}/>
            </List>
        </div>
    </Drawer>
}