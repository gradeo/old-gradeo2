import { createStyles, Theme } from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";

export const usePaperStyling = makeStyles((theme: Theme) =>
    createStyles({
        paper: {
            padding: theme.spacing(2),
        },
    }),
);