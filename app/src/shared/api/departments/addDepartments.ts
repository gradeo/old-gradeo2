import { BASE_URL } from "../base-url";
import { CreateDepartmentDto } from "../../models/departments/CreateDepartmentDto";
import { toast } from "react-toastify";

export async function addDepartment(data: CreateDepartmentDto): Promise<void> {
    try {
        console.log(JSON.stringify(data)); // временная отладка
        await fetch(`${BASE_URL}/api/departments`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Authorization': `Bearer ${sessionStorage.getItem('authentication')}`,
                'Content-Type': 'application/json'
            }
        });
    } catch (e) {
        toast.error("Не удалось сохранить кафедру")
    }
}