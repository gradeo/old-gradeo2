import { DepartmentListItemDto } from "../../models/departments/DepartmentListItemDto";
import {BASE_URL} from "../base-url";

export async function getDepartments(): Promise<DepartmentListItemDto[]> {
    const response = await fetch(`${BASE_URL}/api/departments`,{
        method: 'GET',
        mode: 'cors',
        headers: {
            'Authorization': `Bearer ${sessionStorage.getItem('authentication')}`,
            'Accept'       : 'application/json'
        }
    }
    );
    const items = await response.json();
    return items;
}