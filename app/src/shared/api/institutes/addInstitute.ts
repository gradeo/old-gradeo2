import { BASE_URL } from "../base-url";
import {CreateInstituteDto} from "../../models/institutes/CreateInstituteDto";
import { toast } from "react-toastify";

export async function addInstitute(data: CreateInstituteDto): Promise<void> {
    try {
        await fetch(`${BASE_URL}/api/institutes`,{
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Authorization': `Bearer ${sessionStorage.getItem('authentication')}`,
                'Content-Type': 'application/json'
            }
        });
    } catch (e) {
        toast.error("Не удалось сохранить институт")
    }
}