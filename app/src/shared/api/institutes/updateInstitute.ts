import { BASE_URL } from "../base-url";
import { toast } from "react-toastify";
import { UpdateInstituteDto } from "../../models/institutes/UpdateInstituteDto";

export async function updateInstitute(data: UpdateInstituteDto): Promise<void> {
    try {
        await fetch(`${BASE_URL}/api/institutes`,{
            method: 'PUT',
            body: JSON.stringify(data),
            headers: {
                'Authorization': `Bearer ${sessionStorage.getItem('authentication')}`,
                'Content-Type': 'application/json'
            }
        });
    } catch (e) {
        toast.error("Не удалось обновить институт")
    }
}