import { StudentsListItemDto } from "../models/StudentsListItemDto";
import { BASE_URL } from "./base-url";

export async function getStudents(): Promise<StudentsListItemDto[]> {
    const response = await fetch(`${BASE_URL}/api/students`, {
        method: 'GET',
        mode: 'cors',
        headers: {
            'Authorization': `Bearer ${sessionStorage.getItem('authentication')}`,
            'Accept'       : 'application/json',
        }
    });
    const items = await response.json();
    return items;
}