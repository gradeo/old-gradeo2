export interface UpdateInstituteDto {
    id: number
    name: string
}