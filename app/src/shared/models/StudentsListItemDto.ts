export interface StudentsListItemDto {
    id: number;
    name: string;
    instituteId: number;
    departmentId: number;
    groupId: number;
    instituteName: string;
    departmentName: string;
    groupName: string;
    gradeBook: number;
    enrollmentDate: Date;
    deductionDate: Date;
    educationalForm: string;

}