export interface CreateDepartmentDto {
    name: string;
    instituteId: number;
}