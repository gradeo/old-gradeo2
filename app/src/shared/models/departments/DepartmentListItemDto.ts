export interface DepartmentListItemDto {
    id: number;
    name: string;
    instituteId : number;
    instituteName: string;
}