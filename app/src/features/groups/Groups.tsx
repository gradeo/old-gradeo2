import React, { FC, useState, useEffect } from "react";
import { Typography } from "@material-ui/core";
import { GroupsListItemDto } from "../../shared/models/GroupsListItemDto";
import { getGroups } from "../../shared/api/getGroups";
import { GroupsTable } from "./components/GroupsTable";
import { show as showLoader, hide as hideLoader } from '../../store/slices/loader.slice';
import { useDispatch } from "react-redux";
import { toast } from 'react-toastify';

export const Groups: FC = () => {
    const [items, setItems] = useState<GroupsListItemDto[]>([])
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(showLoader());
        getGroups()
            .then(groups => setItems(groups))
            .catch(()=> toast.error('Не удалось загрузить данные'))
            .finally(() => dispatch(hideLoader()))
    }, [dispatch])

    return (
        <div> 
            <Typography variant="h4" gutterBottom>
            Группы
            </Typography>
            <GroupsTable items={items}/>
        </div>);    
}