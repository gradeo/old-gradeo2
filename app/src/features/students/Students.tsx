import React, { FC, useState, useEffect } from 'react'
import { Typography } from '@material-ui/core';
import { show as showLoader, hide as hideLoader } from '../../store/slices/loader.slice';
import { useDispatch } from "react-redux";
import { toast } from 'react-toastify';
import { StudentsTable } from './components/StudentsTable';
import { StudentsListItemDto } from '../../shared/models/StudentsListItemDto';
import { getStudents } from '../../shared/api/getStudents';

export const Students: FC = () => {
    const [items, setItems] = useState<StudentsListItemDto[]>([])
    const dispatch = useDispatch();
    
    useEffect(() => {
        dispatch(showLoader());
        getStudents()
            .then(students => setItems(students))
            .catch(()=> toast.error('Не удалось загрузить данные'))
            .finally(() => dispatch(hideLoader()))
    }, [dispatch])

    return (
        <div>
            <Typography variant="h4" gutterBottom>
                Студенты
            </Typography>
            <StudentsTable items={items} />
        </div>
    );
}