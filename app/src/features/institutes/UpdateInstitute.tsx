import React, {FC} from 'react';
import {
    Grid,
    Typography,
    Button,
    FormHelperText,
    InputLabel,
    FormControl,
    Input
} from "@material-ui/core";
import {useForm} from "react-hook-form";
import { updateInstitute } from "../../shared/api/institutes/updateInstitute";
import { toast} from "react-toastify";
import { useHistory, useParams } from "react-router-dom";
import { Panel } from '../../shared/components/Panel';

type UpdateInstituteFormData = {
    name: string
}

export const UpdateInstitute: FC = () => {
    let history = useHistory();
    let { id } = useParams();
    const { register, handleSubmit, errors } = useForm<UpdateInstituteFormData>();
    const onSubmit = handleSubmit(async ({ name }) => {
        await updateInstitute({ id, name })
        history.push("/institutes");
        toast.success("Данные успешно сохранены");
    });

    return (
        <form onSubmit={onSubmit}>
            <Panel>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <Typography variant="h6" component="div">
                            Изменение института
                        </Typography>
                    </Grid>
                    <Grid container item xs={6}>
                        <Grid item xs={12}>
                            <FormControl style={{width:"100%"}} error={!!errors.name}>
                                <InputLabel htmlFor="name">Название</InputLabel>
                                <Input
                                    id="name"
                                    name="name"
                                    inputRef={register({required: true})}
                                />
                                { errors.name &&
                                    <FormHelperText id="name-error">
                                        Название не может быть пустым
                                    </FormHelperText>
                                }
                            </FormControl>
                        </Grid>
                    </Grid>
                    <Grid container item xs={12}>
                        <Button type="submit" color="primary">Изменить институт</Button>
                    </Grid>
                </Grid>
            </Panel>
        </form>
    )
}