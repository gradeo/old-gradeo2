import React, {FC} from 'react';
import {
    Grid,
    Typography,
    Button,
    FormHelperText,
    InputLabel,
    FormControl,
    Input
} from "@material-ui/core";
import {useForm} from "react-hook-form";
import {addInstitute} from "../../shared/api/institutes/addInstitute";
import { toast} from "react-toastify";
import { useHistory } from "react-router-dom";
import { Panel } from '../../shared/components/Panel';

type AddInstituteFormData = {
    name: string
}

export const AddInstitute: FC = () => {
    // Позволяет программно совершать редирект на любую страницу
    // Можно также использовать <Redirect to="<path>" />
    // Но он удобен в JSX, но не удобен в обработчиках
    let history = useHistory();

    // Мы используем https://react-hook-form.com/get-started/
    // register служит для регистрации элемента формы внутри хука
    // handleSubmit возвращает функцию для обработки события отправки формы
    // + блокирует нативное событие(event.preventDefault)
    // errors - объект Ключ - Ошибка, где ключи - name полей формы
    // !!errors.name - проверяет, что есть ошибка в поле name и конвертирует результат в boolean
    // Смотрите документацию для более точной валидации
    const { register, handleSubmit, errors } = useForm<AddInstituteFormData>();
    // Обратите внимание, что функция обработки асинхронная
    // И редирект будет ПОСЛЕ успешного запроса на сервер
    const onSubmit = handleSubmit(async ({ name }) => {
        await addInstitute({ name })
        // Редирект обратно на страницу всех институтов
        history.push("/institutes");
        // Показываем уведомление
        toast.success("Данные успешно сохранены");
    });

    return (
        <form onSubmit={onSubmit}>
            <Panel>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <Typography variant="h6" component="div">
                            Новый институт
                        </Typography>
                    </Grid>
                    <Grid container item xs={6}>
                        <Grid item xs={12}>
                            <FormControl style={{width:"100%"}} error={!!errors.name}>
                                <InputLabel htmlFor="name">Название</InputLabel>
                                <Input
                                    id="name"
                                    name="name"
                                    inputRef={register({required: true})}
                                />
                                { errors.name &&
                                    <FormHelperText id="name-error">
                                        Название не может быть пустым
                                    </FormHelperText>
                                }
                            </FormControl>
                        </Grid>
                    </Grid>
                    <Grid container item xs={12}>
                        <Button type="submit" color="primary">Добавить институт</Button>
                    </Grid>
                </Grid>
            </Panel>
        </form>
    )
}