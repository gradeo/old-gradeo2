import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
  table: {
  },
});

function createData(task: string, subject: string, student: string, date: string) {
  return { task, subject, student, date };
}

const rows = [
  createData('ЛР №1', "П:ЯВУ", "Иванова А.А.", "04.09.2020"),
  createData('ЛР №7', "Web", "Быков С.А.", "02.09.2020"),
  createData('ЛР №3', "Web", "Петров П.П.", "02.09.2020"),
  createData('ЛР №5', "П:ЯВУ", "Быков А.А.", "02.09.2020"),
  createData('ЛР №5', "П:ЯВУ", "Быков А.А.", "02.09.2020"),
];

export default function CurrentTasksTable() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Задание</TableCell>
            <TableCell>Предмет</TableCell>
            <TableCell>Студент</TableCell>
            <TableCell>Сдано</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.task}>
              <TableCell component="th" scope="row">
                {row.task}
              </TableCell>
              <TableCell>{row.subject}</TableCell>
              <TableCell>{row.student}</TableCell>
              <TableCell>{row.date}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}