import React, { FC } from 'react'
import { Typography } from '@material-ui/core'

export const About: FC = () => {
    return (
        <Typography variant="h3" gutterBottom>
            About
        </Typography>
    );
}