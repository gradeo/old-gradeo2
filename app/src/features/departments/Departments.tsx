import React, { FC, useState, useEffect } from 'react'
import { Button, Grid, Typography, Breadcrumbs, Link } from '@material-ui/core';
import { DepartmentsTable } from './components/DepartmentsTable';
import { DepartmentListItemDto } from '../../shared/models/departments/DepartmentListItemDto';
import { getDepartments } from '../../shared/api/departments/getDepartments';
import { show as showLoader, hide as hideLoader } from '../../store/slices/loader.slice';
import { useDispatch } from "react-redux";
import { toast } from 'react-toastify';
import { Link as RouterLink } from "react-router-dom";
import AddIcon from "@material-ui/icons/Add";
import { Panel } from '../../shared/components/Panel';
import DepartmentsSearchField from './components/DepartmentsSearchField';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';

export const Departments: FC = () => {
    const [items, setItems] = useState<DepartmentListItemDto[]>([])
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(showLoader());
        getDepartments()
            .then(departments => setItems(departments))
            .catch(() => toast.error('Не удалось загрузить данные'))
            .finally(() => dispatch(hideLoader()))
    }, [dispatch])

    return (
        <div>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Panel>
                        <Grid container item xs={12}>
                            <Grid item xs={8}>
                                <Typography variant="h6">
                                    Кафедры
                            </Typography>
                                <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />} aria-label="breadcrumb">
                                    <Link color="inherit" component={RouterLink} to="/">
                                        Главная
                                    </Link>
                                    <Typography color="textPrimary">Кафедры</Typography>
                                </Breadcrumbs>
                            </Grid>
                            <Grid
                                item
                                container
                                justify="flex-end"
                                alignItems="center"
                                xs={4}
                            >
                                <Grid item>
                                    <Button component={RouterLink}
                                        variant="contained"
                                        color="primary"
                                        disableElevation
                                        to="/departments/new"
                                        startIcon={<AddIcon />}
                                    >
                                        Новая кафедра
                                </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Panel>
                </Grid>
                <Grid item xs={12}>
                    <DepartmentsSearchField />
                </Grid>
                <Grid item xs={12}>
                    <DepartmentsTable items={items} />
                </Grid>
            </Grid>
        </div>
    );
}