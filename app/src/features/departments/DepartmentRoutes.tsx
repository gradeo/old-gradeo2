import React, { FC } from 'react';
import { useRouteMatch, Switch, Route } from "react-router-dom";
import { AddDepartment } from "./AddDepartment";
import { Departments } from "./Departments";

export const DepartmentRoutes: FC = () => {
    let match = useRouteMatch();

    return (
        <Switch>
            <Route path={`${match.path}/new`}>
                <AddDepartment />
            </Route>
            <Route path={match.path}>
                <Departments />
            </Route>
        </Switch>
    );
}