
import React, { useCallback, FC } from 'react'
import { Redirect } from 'react-router-dom'

import { useKeycloak } from '@react-keycloak/web'

export const Login: FC = () => {
  const [keycloak] = useKeycloak()

  const login = useCallback(() => {
    keycloak.login()
  }, [keycloak])

  if (keycloak.authenticated) return <Redirect to="/home" />

  return (
    <div>
      <button type="button" onClick={login}>
        Login
      </button>
    </div>
  );
}