package com.unnamed.gradeo.configuration;

/**
 * Класс с шаблонами строк для логгера
 */
@Deprecated
public class LogStrings {
    // Spring controllers
    public static String GET_REQUEST = "GET '{}', parameters = {}";
    public static String POST_REQUEST = "POST '{}', parameters = {}";
    public static String RETURN_PAGE = "Returning page {}.html";
    public static String BINDING_HAS_ERRORS = "Binding result has errors";
    public static String NO_SUCH = "No such '{}' with id: {}";
    public static String REDIRECT = "Redirecting to {}";
    public static String SEARCH = "Searching for '{}', parameters = id: {}";
    public static String NOT_FOUND = "Couldn't found '{}' with id: {}";
    public static String CREATE = "Creating '{}', parameters = name: '{}'";
    public static String UPDATE = "Updating '{}', parameters = id: {}, name = '{}'";
    public static String DELETE = "Deleting '{}', parameters = id: {}";
    public static String HANDLING_ERROR = "Handling request error with id: {}";


    // Services
    public static String GET_ALL_BY_REMOVED_DATE_IS_NULL = "Getting all '{}', parameters = removedDate is null";
    public static String FIND_BY_ID = "Searching '{}', parameters = id: {}";
    public static String SAVE = "Saving to repository, parameters = id {}, name = '{}'";
}
