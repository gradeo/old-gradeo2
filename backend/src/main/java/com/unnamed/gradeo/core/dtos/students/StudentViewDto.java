package com.unnamed.gradeo.core.dtos.students;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StudentViewDto {
    private Integer id;

    private String name;

    private Integer instituteId;

    private Integer departmentId;

    private Integer groupId;

    private Integer semesterId;

    private String instituteName;

    private String departmentName;

    private String groupName;

    private Integer semesterNumber;

    private Integer gradeBook;

    private LocalDate enrollmentDate;

    private LocalDate deductionDate;

    private String educationalForm;
}
