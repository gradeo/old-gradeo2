package com.unnamed.gradeo.core.exceptions.entities;

import com.unnamed.gradeo.core.exceptions.EntityNotFoundException;

public class NoSuchTaskException extends EntityNotFoundException {
    @Override
    public String getError() {
        return "Task is not found";
    }
}
