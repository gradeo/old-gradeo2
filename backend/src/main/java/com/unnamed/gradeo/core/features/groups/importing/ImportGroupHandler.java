package com.unnamed.gradeo.core.features.groups.importing;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.core.abstractions.ImportUtil;
import com.unnamed.gradeo.core.exceptions.ImportIOException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@Slf4j
@Component
@AllArgsConstructor
public class ImportGroupHandler implements Command.Handler<ImportGroupCommand, Voidy> {

    private final ImportUtil importLib;

    @Override
    public Voidy handle(ImportGroupCommand command) {
        try {
            importGroups(command.getFile());
        } catch (IOException e) {
            throw new ImportIOException();
        }
        return new Voidy();
    }

    private void importGroups(MultipartFile file) throws IOException {
        var workbook = importLib.checkType(file);
        var sheet = workbook.getSheetAt(0);
        var rowIterator = sheet.iterator();
        if (rowIterator.hasNext()) rowIterator.next();

        Map<Integer, String> instituteCache = importLib.getInstituteCache();
        Map<Integer, String> departmentCache = importLib.getDepartmentCache();
        Map<Integer, String> groupCache = importLib.getGroupCache();

        while (rowIterator.hasNext()) {
            var cellIterator = rowIterator.next().cellIterator();
            var instituteName = cellIterator.next().getStringCellValue();
            var departmentName = cellIterator.next().getStringCellValue();
            var groupName = cellIterator.next().getStringCellValue();

            if (groupCache.containsValue(groupName)) {
                log.debug("Попытка импорта группы '{}' - группа уже существует.", groupName);
            } else {
                // если в кэше нет необходимой кафедры
                if (!departmentCache.containsValue(departmentName)) {
                    //если в кэше нет необходимого института
                    if (!instituteCache.containsValue(instituteName)) {
                        importLib.createInstitute(instituteCache, instituteName);
                    }
                    importLib.createDepartment(departmentCache, departmentName, instituteName);
                }
                importLib.createGroup(groupCache,
                        groupName, departmentName);
            }

        }
    }
}
