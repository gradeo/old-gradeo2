package com.unnamed.gradeo.core.entities;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "groups")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
//на данный момент основная задача групп - объединять "семестры"
public class Group extends BaseEntity {

    //выды групп
    public enum Type {
        //магистратура
        MAGISTRACY,
        //бакалавриат
        BACCALAUREATE,
        //Специалитет
        SPECIALIST_DEGREE
    }

    @Enumerated(EnumType.STRING)
    private Group.Type type;

    @OneToMany(mappedBy = "group")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Semester> semesters; //семестры

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    @Transient
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Speciality speciality;

    private String name;

    private String numberGroup;

  /*  @OneToOne(targetEntity = Teacher.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "teacher_id")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Teacher teacher; //куратор*/

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private LocalDate formedDate;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "department_id", nullable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Department department;
}
