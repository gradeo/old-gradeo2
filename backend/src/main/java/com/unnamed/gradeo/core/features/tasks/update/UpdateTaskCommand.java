package com.unnamed.gradeo.core.features.tasks.update;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.core.entities.Task;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UpdateTaskCommand implements Command<Voidy> {
    @NotNull(message = "Не выбран id сущности")
    private Integer id;

    @Size(min = 2, message = "Введите задание")
    private String name;

    @NotNull(message = "Выберите тип задания")
    private Task.Type type;

    @Positive
    @NotNull(message = "Выберите предмет, к которому относится задание")
    private Integer subjectId;
}
