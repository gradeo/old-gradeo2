package com.unnamed.gradeo.core.entities;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "semesters")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
//класс-представление семестра для группы
public class Semester extends BaseEntity{

    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    @Column
    private Integer id;

    private Integer number; //номер семестра (первый/второй/...)

    @OneToMany(mappedBy = "semester")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Student> students; //студенты в группе на период семестра

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "group_id", nullable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Group group;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    //дата начала семестра
    private LocalDate startDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    //дата конца семестра
    private LocalDate endDate;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "semesters_subjects",
            joinColumns = @JoinColumn(name = "semester_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "subject_id", referencedColumnName = "id"))
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Subject> subjects; //предметы, изучаемые группой

    public String[] getStudentsNames(){
        return students.stream().map(Student::getName).toArray(String[]::new);
    }

    public String[] getSubjectsNames(){
        return subjects.stream().map(Subject::getName).toArray(String[]::new);
    }
}
