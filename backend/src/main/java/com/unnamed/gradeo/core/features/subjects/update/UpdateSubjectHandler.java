package com.unnamed.gradeo.core.features.subjects.update;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchDepartmentException;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchSubjectException;
import com.unnamed.gradeo.repositories.DepartmentsRepository;
import com.unnamed.gradeo.repositories.SubjectsRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class UpdateSubjectHandler implements Command.Handler<UpdateSubjectCommand, Voidy> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final DepartmentsRepository departmentsRepository;
    private final SubjectsRepository repository;

    @Override
    public Voidy handle(UpdateSubjectCommand command) {
        logger.debug(LogStrings.FIND_BY_ID, "Subject", command.getId());
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchSubjectException::new);

        logger.debug(LogStrings.FIND_BY_ID, "Department", command.getDepartmentId());
        var department = departmentsRepository
                .findByIdAndRemovedDateIsNull(command.getDepartmentId())
                .orElseThrow(NoSuchDepartmentException::new);

        entity.setDepartment(department);
        entity.setName(command.getName());
        entity.setUpdatedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        return new Voidy();
    }
}
