package com.unnamed.gradeo.core.features.institutes.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.institutes.InstituteViewDto;
import com.unnamed.gradeo.repositories.InstituteRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
//выдача списка всех институтов
public class GetInstitutesHandler implements Command.Handler<GetInstitutesQuery, List<InstituteViewDto>> {

    private final InstituteRepository repository;
    private final ModelMapper mapper;

    public GetInstitutesHandler(@NotNull InstituteRepository repository, ModelMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    //получает пустую команду, и возвращает лист с InstituteViewDto всех институтов
    public List<InstituteViewDto> handle(GetInstitutesQuery command) {
        var entities = repository.findAllByRemovedDateIsNullOrderByIdAsc();
        return StreamSupport.stream(entities.spliterator(), false)
                .map(entity -> mapper.map(entity, InstituteViewDto.class))
                .collect(Collectors.toList());
    }
}
