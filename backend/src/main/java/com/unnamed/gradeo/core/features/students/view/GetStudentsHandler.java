package com.unnamed.gradeo.core.features.students.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.students.StudentViewDto;
import com.unnamed.gradeo.core.entities.Student;
import com.unnamed.gradeo.repositories.StudentsRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
@AllArgsConstructor
public class GetStudentsHandler implements Command.Handler<GetStudentsQuery, List<StudentViewDto>> {

    private final ModelMapper mapper;
    private final StudentsRepository repository;


    @Override
    public List<StudentViewDto> handle(GetStudentsQuery command) {
        var entities = repository.findAllByRemovedDateIsNullOrderByIdAsc();
        System.out.println(entities.toString());
        return StreamSupport.stream(entities.spliterator(), false)
                .map(entity -> mapper.map(entity, StudentViewDto.class))
                .collect(Collectors.toList());
    }

    @PostConstruct
    private void configureMappings() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

        this.mapper.typeMap(Student.class, StudentViewDto.class)
                .addMappings(mapper -> mapper.map(Student::getGradeBook, StudentViewDto::setGradeBook))
                .addMappings(mapper -> mapper.map(d -> d.getSemester().getNumber(), StudentViewDto::setSemesterNumber))
                .addMappings(mapper -> mapper.map(d -> d.getSemester().getId(), StudentViewDto::setSemesterId))
                .addMappings(mapper -> mapper.map(d -> d.getSemester().getGroup().getName(), StudentViewDto::setGroupName))
                .addMappings(mapper -> mapper.map(d -> d.getSemester().getGroup().getId(), StudentViewDto::setGroupId))
                .addMappings(mapper -> mapper.map(d -> d.getSemester().getGroup().getDepartment().getName(), StudentViewDto::setDepartmentName))
                .addMappings(mapper -> mapper.map(d -> d.getSemester().getGroup().getDepartment().getInstitute().getName(), StudentViewDto::setInstituteName))
                .addMappings(mapper -> mapper.map(d -> d.getSemester().getGroup().getDepartment().getId(), StudentViewDto::setDepartmentId))
                .addMappings(mapper -> mapper.map(d -> d.getSemester().getGroup().getDepartment().getInstitute().getId(), StudentViewDto::setInstituteId))
                .addMappings(mapper -> mapper.map(Student::getDeductionDate, StudentViewDto::setDeductionDate));
    }
}
