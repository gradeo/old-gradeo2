package com.unnamed.gradeo.core.dtos.subjects;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SubjectViewDto {
    private Integer id;
    private String name;
    private Integer instituteId;
    private Integer departmentId;
    private Integer[] teachersId;
    private String instituteName;
    private String departmentName;
    private String[] teachersNames;
}
