package com.unnamed.gradeo.core.features.lessons.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.lessons.LessonViewDto;
import com.unnamed.gradeo.core.entities.Lesson;
import com.unnamed.gradeo.repositories.LessonsRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
@AllArgsConstructor
public class GetLessonsHandler implements Command.Handler<GetLessonsQuery, List<LessonViewDto>> {

    private final ModelMapper mapper;
    private final LessonsRepository repository;


    @Override
    public List<LessonViewDto> handle(GetLessonsQuery command) {
        var entities = repository.findAllByRemovedDateIsNullOrderByIdAsc();
        System.out.println(entities.toString());
        return StreamSupport.stream(entities.spliterator(), false)
                .map(entity -> mapper.map(entity, LessonViewDto.class))
                .collect(Collectors.toList());
    }

    @PostConstruct
    private void configureMappings() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        this.mapper.typeMap(Lesson.class, LessonViewDto.class)
                .addMappings(mapper -> mapper.map(d -> d.getSemester().getGroup().getDepartment().getId(), LessonViewDto::setDepartmentId))
                .addMappings(mapper -> mapper.map(d -> d.getSemester().getGroup().getDepartment().getName(), LessonViewDto::setDepartmentName))
                .addMappings(mapper -> mapper.map(d -> d.getSemester().getGroup().getDepartment().getInstitute().getId(), LessonViewDto::setInstituteId))
                .addMappings(mapper -> mapper.map(d -> d.getSemester().getGroup().getDepartment().getInstitute().getName(), LessonViewDto::setInstituteName))
                .addMappings(mapper -> mapper.map(d -> d.getSemester().getGroup().getId(), LessonViewDto::setGroupId))
                .addMappings(mapper -> mapper.map(d -> d.getSemester().getGroup().getName(), LessonViewDto::setGroupName))
                .addMappings(mapper -> mapper.map(d -> d.getSubject().getId(), LessonViewDto::setSubjectId))
                .addMappings(mapper -> mapper.map(d -> d.getSubject().getName(), LessonViewDto::setSubjectName))
                .addMappings(mapper -> mapper.map(d -> d.getSemester().getId(), LessonViewDto::setSemesterId))
                .addMappings(mapper -> mapper.map(d -> d.getSemester().getNumber(), LessonViewDto::setSemesterNumber))
                .addMappings(mapper -> mapper.map(Lesson::getAuditorium, LessonViewDto::setAuditorium))
                .addMappings(mapper -> mapper.map(Lesson::getDate, LessonViewDto::setDate));
    }
}
