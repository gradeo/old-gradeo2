package com.unnamed.gradeo.core.features.tasks.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.dtos.tasks.TaskViewDto;
import com.unnamed.gradeo.core.entities.Task;
import com.unnamed.gradeo.repositories.TaskRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
@AllArgsConstructor
public class GetTasksHandler implements Command.Handler<GetTasksQuery, List<TaskViewDto>> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final TaskRepository repository;
    private final ModelMapper mapper;


    @Override
    public List<TaskViewDto> handle(GetTasksQuery command) {
        logger.debug(LogStrings.GET_ALL_BY_REMOVED_DATE_IS_NULL, "Tasks");
        var tasks = repository.findAllByRemovedDateIsNullOrderByIdAsc();
        return StreamSupport.stream(tasks.spliterator(), false)
                .map(entity -> mapper.map(entity, TaskViewDto.class))
                .collect(Collectors.toList());
    }

    @PostConstruct
    public void configureMapping() {
        this.mapper.typeMap(Task.class, TaskViewDto.class)
                .addMappings(mapper -> mapper.map(Task::getType, TaskViewDto::setType))
                .addMappings(mapper -> mapper.map(d -> d.getSubject().getId(), TaskViewDto::setSubjectId))
                .addMappings(mapper -> mapper.map(d -> d.getSubject().getName(), TaskViewDto::setSubjectName));
    }
}
