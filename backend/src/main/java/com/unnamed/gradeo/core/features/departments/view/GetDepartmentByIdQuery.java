package com.unnamed.gradeo.core.features.departments.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.departments.DepartmentViewDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@AllArgsConstructor
public class GetDepartmentByIdQuery implements Command<DepartmentViewDto> {

    @NotNull(message = "Не выбран id сущности")
    @Positive
    private final Integer id;
}
