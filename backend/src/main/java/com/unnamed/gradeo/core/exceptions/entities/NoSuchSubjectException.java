package com.unnamed.gradeo.core.exceptions.entities;

import com.unnamed.gradeo.core.exceptions.EntityNotFoundException;

public class NoSuchSubjectException extends EntityNotFoundException {
    @Override
    public String getError() {
        return "Subject is not found";
    }
}
