package com.unnamed.gradeo.core.features.tasks.create;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.entities.Task;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchSubjectException;
import com.unnamed.gradeo.repositories.SubjectsRepository;
import com.unnamed.gradeo.repositories.TaskRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class CreateTaskHandler implements Command.Handler<CreateTaskCommand, Voidy> {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final ModelMapper mapper;
    private final SubjectsRepository subjectsRepository;
    private final TaskRepository repository;

    @Override
    public Voidy handle(CreateTaskCommand command) {
        logger.debug("Mapping to Task.class");
        var entity = mapper.map(command, Task.class);
        logger.debug(LogStrings.FIND_BY_ID, "Subject", command.getSubjectId());
        var subject = subjectsRepository
                .findByIdAndRemovedDateIsNull(command.getSubjectId())
                .orElseThrow(NoSuchSubjectException::new);
        entity.setName(command.getName());
        entity.setSubject(subject);
        entity.setType(command.getType());
        entity.setCreatedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        return new Voidy();
    }

    @PostConstruct
    private void configureMappings() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
    }
}
