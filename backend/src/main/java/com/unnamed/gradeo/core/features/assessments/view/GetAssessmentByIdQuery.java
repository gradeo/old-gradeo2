package com.unnamed.gradeo.core.features.assessments.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.assessments.AssessmentViewDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@AllArgsConstructor
public class GetAssessmentByIdQuery implements Command<AssessmentViewDto> {

    @NotNull(message = "Не выбран id сущности")
    @Positive
    private final Integer id;
}
