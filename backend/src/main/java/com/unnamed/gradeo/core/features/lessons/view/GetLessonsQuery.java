package com.unnamed.gradeo.core.features.lessons.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.lessons.LessonViewDto;

import java.util.List;

public class GetLessonsQuery implements Command<List<LessonViewDto>> {
}
