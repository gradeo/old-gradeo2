package com.unnamed.gradeo.core.features.groups.update;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchDepartmentException;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchGroupException;
import com.unnamed.gradeo.repositories.DepartmentsRepository;
import com.unnamed.gradeo.repositories.GroupsRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class UpdateGroupHandler implements Command.Handler<UpdateGroupCommand, Voidy> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final DepartmentsRepository departmentsRepository;
    private final GroupsRepository repository;

    @Override
    public Voidy handle(UpdateGroupCommand command) {
        logger.debug(LogStrings.FIND_BY_ID, "Group", command.getId());
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchGroupException::new);

        logger.debug(LogStrings.FIND_BY_ID, "Department", command.getDepartmentId());
        var department = departmentsRepository
                .findByIdAndRemovedDateIsNull(command.getDepartmentId())
                .orElseThrow(NoSuchDepartmentException::new);

        entity.setName(command.getName());
        entity.setDepartment(department);
        entity.setFormedDate(command.getFormedDate());
        entity.setUpdatedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        return new Voidy();
    }
}
