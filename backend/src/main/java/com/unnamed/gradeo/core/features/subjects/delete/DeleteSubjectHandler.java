package com.unnamed.gradeo.core.features.subjects.delete;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchSubjectException;
import com.unnamed.gradeo.repositories.SubjectsRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class DeleteSubjectHandler implements Command.Handler<DeleteSubjectCommand, Voidy> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final SubjectsRepository repository;

    @Override
    public Voidy handle(DeleteSubjectCommand command) {
        logger.debug(LogStrings.FIND_BY_ID, "Subject", command.getId());
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchSubjectException::new);

        entity.setRemovedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        return new Voidy();
    }
}
