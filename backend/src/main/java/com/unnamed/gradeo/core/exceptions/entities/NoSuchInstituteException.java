package com.unnamed.gradeo.core.exceptions.entities;

import com.unnamed.gradeo.core.exceptions.EntityNotFoundException;

public class NoSuchInstituteException extends EntityNotFoundException {
    @Override
    public String getError() {
        return "Institute is not found";
    }
}
