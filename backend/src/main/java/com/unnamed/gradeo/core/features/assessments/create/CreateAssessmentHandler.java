package com.unnamed.gradeo.core.features.assessments.create;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.entities.Assessment;
import com.unnamed.gradeo.core.exceptions.entities.AssessmentAlreadyExistException;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchStudentException;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchTaskException;
import com.unnamed.gradeo.repositories.AssessmentsRepository;
import com.unnamed.gradeo.repositories.StudentsRepository;
import com.unnamed.gradeo.repositories.TaskRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class CreateAssessmentHandler implements Command.Handler<CreateAssessmentCommand, Voidy> {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final ModelMapper mapper;
    private final StudentsRepository studentsRepository;
    private final TaskRepository taskRepository;
    private final AssessmentsRepository repository;


    @Override
    public Voidy handle(CreateAssessmentCommand command) {
        logger.debug("Mapping to Assessment.class");
        var entity = mapper.map(command, Assessment.class);
        var checkAssessment = repository
                .findByTaskIdAndStudentIdAndRemovedDateIsNull
                        (command.getTaskId(), command.getStudentId());

        if (checkAssessment.isPresent()) {
            throw new AssessmentAlreadyExistException(); //TODO: не выводится сообщение об ошибке, вместо этого status 500
        } else {
            logger.debug(LogStrings.FIND_BY_ID, "Task", command.getTaskId());
            var task = taskRepository
                    .findByIdAndRemovedDateIsNull(command.getTaskId())
                    .orElseThrow(NoSuchTaskException::new);

            entity.setTask(task);
            logger.debug(LogStrings.FIND_BY_ID, "Student", command.getStudentId());
            var student = studentsRepository
                    .findByIdAndRemovedDateIsNull(command.getStudentId())
                    .orElseThrow(NoSuchStudentException::new);

            entity.setStudent(student);
            entity.setScore(command.getScore());
            entity.setCreatedDate(LocalDateTime.now());
            repository.save(entity);
        }
        return new Voidy();
    }
}
