package com.unnamed.gradeo.core.features.groups.delete;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchGroupException;
import com.unnamed.gradeo.repositories.GroupsRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class DeleteGroupHandler implements Command.Handler<DeleteGroupCommand, Voidy> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final GroupsRepository repository;

    @Override
    public Voidy handle(DeleteGroupCommand command) {
        logger.debug(LogStrings.FIND_BY_ID, "Group", command.getId());
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchGroupException::new);
        logger.debug("Setting removedDate");
        entity.setRemovedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        return new Voidy();
    }
}
