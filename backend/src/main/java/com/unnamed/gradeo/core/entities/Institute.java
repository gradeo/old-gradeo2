package com.unnamed.gradeo.core.entities;

import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Данный класс описывает сущность "Институт"
 */
@Entity
@Table(name = "institutes")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
public class Institute extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id = 0;

    @NonNull
    private String name;

    @OneToMany(mappedBy = "institute")
    @Where(clause = "removed_date is null")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Department> departments = new HashSet<>();


}
