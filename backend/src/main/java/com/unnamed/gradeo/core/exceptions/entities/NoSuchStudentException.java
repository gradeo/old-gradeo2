package com.unnamed.gradeo.core.exceptions.entities;

import com.unnamed.gradeo.core.exceptions.EntityNotFoundException;

public class NoSuchStudentException extends EntityNotFoundException {
    @Override
    public String getError() {
        return "Student is not found";
    }
}
