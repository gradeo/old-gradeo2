package com.unnamed.gradeo.core.features.institutes.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.institutes.InstituteViewDto;

import java.util.List;

//выдача списка всех институтов
public class GetInstitutesQuery implements Command<List<InstituteViewDto>> {
}
