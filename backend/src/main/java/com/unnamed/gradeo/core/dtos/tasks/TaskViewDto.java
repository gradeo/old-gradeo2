package com.unnamed.gradeo.core.dtos.tasks;

import com.unnamed.gradeo.core.entities.Task;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TaskViewDto {
    private Integer id;
    private String name;
    private Task.Type type;
    private Integer subjectId;
    private String subjectName;
}
