package com.unnamed.gradeo.core.features.groups.create;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.entities.Group;
import com.unnamed.gradeo.core.entities.Semester;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchDepartmentException;
import com.unnamed.gradeo.repositories.DepartmentsRepository;
import com.unnamed.gradeo.repositories.GroupsRepository;
import com.unnamed.gradeo.repositories.SemestersRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Component
@AllArgsConstructor
public class CreateGroupHandler implements Command.Handler<CreateGroupCommand, Voidy> {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final ModelMapper mapper;
    private final DepartmentsRepository departmentsRepository;
    private final GroupsRepository repository;
    private final SemestersRepository semestersRepository;

    @Override
    public Voidy handle(CreateGroupCommand command) {
        logger.debug("Mapping to Group.class");
        var entity = mapper.map(command, Group.class);
        logger.debug(LogStrings.FIND_BY_ID, "Department", command.getDepartmentId());
        var department = departmentsRepository
                .findByIdAndRemovedDateIsNull(command.getDepartmentId())
                .orElseThrow(NoSuchDepartmentException::new);

        entity.setDepartment(department);
        entity.setCreatedDate(LocalDateTime.now());
        entity.setType(command.getType());
        //семестры создаются при создании группы, а редактируются отдельно
        Set<Semester> semesters = new HashSet<>();
        repository.save(entity);
        for(int i = 1; i <= command.getNumOfSemesters(); i++){
            Semester semester = new Semester();
            semester.setNumber(i);
            semester.setGroup(entity);
            semester.setCreatedDate(LocalDateTime.now());
            semester = semestersRepository.save(semester);
            semesters.add(semester);
        }
        entity.setSemesters(semesters);
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        return new Voidy();
    }

    @PostConstruct
    private void configureMappings() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
    }
}
