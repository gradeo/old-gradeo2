package com.unnamed.gradeo.core.exceptions;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImportIOException extends BusinessLogicException {

    public ImportIOException() {
        super();
    }

    public String getError() {
        return "Error while reading import file - IO Exception";
    }

    public ImportIOException(String message) {
        super(message);
    }
}
