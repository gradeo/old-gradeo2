package com.unnamed.gradeo.core.features.students.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.students.StudentViewDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@AllArgsConstructor
public class GetStudentByIdQuery implements Command<StudentViewDto> {

    @NotNull(message = "Не выбран id сущности")
    @Positive
    private final Integer id;
}
