package com.unnamed.gradeo.core.entities;

import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "lessons")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
public class Lesson extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id = 0;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "subject_id", nullable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Subject subject;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "semester_id", nullable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Semester semester;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private LocalDateTime date;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "teacher_id", nullable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Teacher teacher;

    @OneToMany(mappedBy = "lesson", cascade = CascadeType.ALL)
    @Where(clause = "removed_date is null")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Attendance> attendance;

    private String auditorium;

    @NonNull
    private String name;     //имя = названию предмета
}
