package com.unnamed.gradeo.core.features.lessons.create;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CreateLessonCommand implements Command<Voidy> {

    @Positive
    @NotNull(message = "Выберите предмет")
    private Integer subjectId;

    @Positive
    @NotNull(message = "Выберите семестр")
    private Integer semesterId;

    @NotNull(message = "Выберите дату проведения занятия")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;

    @NotNull(message = "Выберите время проведения занятия")
    @DateTimeFormat(pattern = "HH:mm")
    private LocalTime time;

    @NotNull(message = "Введите аудиторию")
    private String auditorium;
}
