package com.unnamed.gradeo.core.features.attendance.create;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

/**
 * Массивы хранят только присутствующих студентов.
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CreateAttendanceCommand implements Command<Voidy> {
    @Positive
    @NotNull(message = "Выберите занятие")
    private Integer lessonId;

    @Positive
    @NotNull(message = "Выберите семестр")
    private Integer semesterId;

    private Integer[] studentsId;
}
