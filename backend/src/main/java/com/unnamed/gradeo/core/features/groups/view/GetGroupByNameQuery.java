package com.unnamed.gradeo.core.features.groups.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.groups.GroupViewDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
@AllArgsConstructor
public class GetGroupByNameQuery implements Command<GroupViewDto> {

    @NotNull(message = "Введите имя группы")
    private String name;
}
