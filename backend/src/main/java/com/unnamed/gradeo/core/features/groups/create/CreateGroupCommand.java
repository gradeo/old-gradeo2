package com.unnamed.gradeo.core.features.groups.create;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.core.entities.Group;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateGroupCommand implements Command<Voidy> {
    @Size(min = 2, message = "Введите название группы")
    private String name;

    @Positive
    @NotNull(message = "Выберите кафедру")
    private Integer departmentId;

    //TODO: временное отключение
    //@NotNull(message = "Введите дату образования группы")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate formedDate;

    @Positive
    @NotNull(message = "Введите количество семестров")
    private Integer numOfSemesters;

    @NotNull(message = "Выберите тип группы")
    private Group.Type type;
}
