package com.unnamed.gradeo.core.exceptions.entities;

import com.unnamed.gradeo.core.exceptions.EntityNotFoundException;

public class NoSuchSemesterException extends EntityNotFoundException {
    @Override
    public String getError() {
        return "Semester is not found";
    }
}
