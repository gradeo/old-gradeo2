package com.unnamed.gradeo.core.features.groups.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.groups.GroupViewDto;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchGroupException;
import com.unnamed.gradeo.repositories.GroupsRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class GetGroupByIdHandler implements Command.Handler<GetGroupByIdQuery, GroupViewDto> {

    private final GroupsRepository repository;
    private final ModelMapper mapper;

    public GetGroupByIdHandler(GroupsRepository repository, ModelMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public GroupViewDto handle(GetGroupByIdQuery command) {
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchGroupException::new);
        return mapper.map(entity, GroupViewDto.class);
    }
}
