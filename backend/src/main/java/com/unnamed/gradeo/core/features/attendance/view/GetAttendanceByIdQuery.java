package com.unnamed.gradeo.core.features.attendance.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.attendance.AttendanceViewDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@AllArgsConstructor
public class GetAttendanceByIdQuery implements Command<AttendanceViewDto> {

    @NotNull(message = "Не выбран id сущности")
    @Positive
    private final Integer id;
}
