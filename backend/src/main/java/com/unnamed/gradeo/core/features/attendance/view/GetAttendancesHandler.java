package com.unnamed.gradeo.core.features.attendance.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.dtos.attendance.AttendanceViewDto;
import com.unnamed.gradeo.core.entities.Attendance;
import com.unnamed.gradeo.repositories.AttendanceRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
@AllArgsConstructor
public class GetAttendancesHandler implements Command.Handler<GetAttendancesQuery, List<AttendanceViewDto>> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final ModelMapper mapper;
    private final AttendanceRepository repository;

    @Override
    public List<AttendanceViewDto> handle(GetAttendancesQuery command) {
        logger.debug(LogStrings.GET_ALL_BY_REMOVED_DATE_IS_NULL, "Attendance");
        var entities = repository
                .findAllByRemovedDateIsNullOrderByIdAsc();

        return StreamSupport.stream(entities.spliterator(), false)
                .map(entity -> mapper.map(entity, AttendanceViewDto.class))
                .collect(Collectors.toList());
    }

    @PostConstruct
    void configureMappings() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        this.mapper.typeMap(Attendance.class, AttendanceViewDto.class)
                .addMappings(mapper -> mapper.map(d -> d.getLesson().getSubject().getName(), AttendanceViewDto::setSubjectName))
                .addMappings(mapper -> mapper.map(d -> d.getLesson().getSubject().getId(), AttendanceViewDto::setSubjectId))
                .addMappings(mapper -> mapper.map(d -> d.getLesson().getSemester().getId(), AttendanceViewDto::setSemesterId))
                .addMappings(mapper -> mapper.map(d -> d.getLesson().getSemester().getNumber(), AttendanceViewDto::setSemesterNumber))
                .addMappings(mapper -> mapper.map(d -> d.getLesson().getSemester().getGroup().getName(), AttendanceViewDto::setGroupName))
                .addMappings(mapper -> mapper.map(d -> d.getLesson().getSemester().getGroup().getName(), AttendanceViewDto::setGroupName))
                .addMappings(mapper -> mapper.map(d -> d.getLesson().getId(), AttendanceViewDto::setLessonId))
                .addMappings(mapper -> mapper.map(d -> d.getLesson().getName(), AttendanceViewDto::setLessonName))
                .addMappings(mapper -> mapper.map(d -> d.getStudent().getId(), AttendanceViewDto::setStudentId))
                .addMappings(mapper -> mapper.map(d -> d.getStudent().getName(), AttendanceViewDto::setStudentName))
                .addMappings(mapper -> mapper.map(Attendance::getAttendanceStatus, AttendanceViewDto::setAttendanceStatus));
    }

}
