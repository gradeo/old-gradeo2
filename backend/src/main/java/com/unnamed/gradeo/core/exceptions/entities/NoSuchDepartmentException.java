package com.unnamed.gradeo.core.exceptions.entities;

import com.unnamed.gradeo.core.exceptions.EntityNotFoundException;

public class NoSuchDepartmentException extends EntityNotFoundException {
    @Override
    public String getError() {
        return "Department is not found";
    }
}
