package com.unnamed.gradeo.core.features.lessons.create;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.entities.Lesson;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchSemesterException;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchSubjectException;
import com.unnamed.gradeo.repositories.LessonsRepository;
import com.unnamed.gradeo.repositories.SemestersRepository;
import com.unnamed.gradeo.repositories.SubjectsRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class CreateLessonHandler implements Command.Handler<CreateLessonCommand, Voidy> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final ModelMapper mapper;
    private final SemestersRepository semestersRepository;
    private final SubjectsRepository subjectsRepository;
    private final LessonsRepository repository;

    @Override
    public Voidy handle(CreateLessonCommand command) {
        logger.debug("Mapping to Lesson.class");
        var entity = mapper.map(command, Lesson.class);
        logger.debug(LogStrings.FIND_BY_ID, "Semester", command.getSemesterId());
        var semester = semestersRepository
                .findByIdAndRemovedDateIsNull(command.getSemesterId())
                .orElseThrow(NoSuchSemesterException::new);
        logger.debug(LogStrings.FIND_BY_ID, "Subject", command.getSubjectId());
        var subject = subjectsRepository
                .findByIdAndRemovedDateIsNull(command.getSubjectId())
                .orElseThrow(NoSuchSubjectException::new);

        entity.setName(subject.getName());
        entity.setSemester(semester);
        entity.setSubject(subject);
        entity.setDate(LocalDateTime.of(command.getDate(), command.getTime()));
        entity.setAuditorium(command.getAuditorium());
        entity.setCreatedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        return new Voidy();
        //TODO: addLessonsTable.sql teacher_id not null -> завести преподов и исправить
    }

    @PostConstruct
    private void configureMappings() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
    }
}
