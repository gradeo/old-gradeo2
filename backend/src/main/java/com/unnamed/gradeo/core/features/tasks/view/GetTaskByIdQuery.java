package com.unnamed.gradeo.core.features.tasks.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.tasks.TaskViewDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@AllArgsConstructor
public class GetTaskByIdQuery implements Command<TaskViewDto> {

    @NotNull(message = "Не выбран id сущности")
    @Positive
    private final Integer id;
}
