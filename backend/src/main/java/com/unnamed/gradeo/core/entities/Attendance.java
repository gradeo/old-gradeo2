package com.unnamed.gradeo.core.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Класс-сущность "Посещаемость", описывающий посещение <b>Student</b> занятия <b>Lesson</b> со статусом <b>AttendanceStatus</b>.
 */
@Entity
@Table(name = "attendance")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
public class Attendance extends BaseEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id = 0;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Student student;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Lesson lesson;

    @Enumerated(EnumType.STRING)
    private AttendanceStatus attendanceStatus;

}
