package com.unnamed.gradeo.core.features.assessments.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.dtos.assessments.AssessmentViewDto;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchAssessmentException;
import com.unnamed.gradeo.repositories.AssessmentsRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class GetAssessmentByIdHandler implements Command.Handler<GetAssessmentByIdQuery, AssessmentViewDto> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final AssessmentsRepository repository;
    private final ModelMapper mapper;

    @Override
    public AssessmentViewDto handle(GetAssessmentByIdQuery command) {
        logger.debug(LogStrings.FIND_BY_ID, "Assessment", command.getId());
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchAssessmentException::new);

        return mapper.map(entity, AssessmentViewDto.class);
    }
}
