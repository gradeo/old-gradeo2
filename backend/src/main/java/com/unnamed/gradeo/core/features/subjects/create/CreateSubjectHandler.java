package com.unnamed.gradeo.core.features.subjects.create;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.entities.Subject;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchDepartmentException;
import com.unnamed.gradeo.repositories.DepartmentsRepository;
import com.unnamed.gradeo.repositories.SubjectsRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class CreateSubjectHandler implements Command.Handler<CreateSubjectCommand, Voidy> {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final ModelMapper mapper;
    private final DepartmentsRepository departmentsRepository;
    private final SubjectsRepository repository;

    @Override
    public Voidy handle(CreateSubjectCommand command) {
        logger.debug("Mapping to Subject.class");
        var entity = mapper.map(command, Subject.class);
        logger.debug(LogStrings.FIND_BY_ID, "Department", command.getDepartmentId());
        var department = departmentsRepository
                .findByIdAndRemovedDateIsNull(command.getDepartmentId())
                .orElseThrow(NoSuchDepartmentException::new);

        entity.setDepartment(department);
        entity.setCreatedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        return new Voidy();
    }

    @PostConstruct
    private void configureMappings() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
    }
}
