package com.unnamed.gradeo.core.dtos.semesters;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SemesterViewDto {
    private Integer id;
    private Integer number;
    private String[] studentsNames;
    private Integer groupId;
    private LocalDate startDate;
    private LocalDate endDate;
    private String[] subjectsNames;
}
