package com.unnamed.gradeo.core.features.students.delete;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchStudentException;
import com.unnamed.gradeo.repositories.StudentsRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class DeleteStudentHandler implements Command.Handler<DeleteStudentCommand, Voidy> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final StudentsRepository repository;

    @Override
    public Voidy handle(DeleteStudentCommand command) {
        logger.debug(LogStrings.FIND_BY_ID, "Student", command.getId());
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchStudentException::new);

        entity.setRemovedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        return new Voidy();
    }
}
