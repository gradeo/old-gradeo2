package com.unnamed.gradeo.core.entities;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "tasks")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
public class Task extends BaseEntity {
    public enum Type {
        LABORATORY, // лабораторная работа
        PRACTICAL, // практическая работа
        COURSE, // курсовая работа
        TEST, // контрольная работа
        GRADUATE, // дипломная работа
        PAYMENT_AND_GRAPHIC, // РГЗ
        ESSAY, // реферат/доклад
        OTHER // другое
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Task.Type type;

    @NonNull
    private String name;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "subject_id", nullable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Subject subject;

}
