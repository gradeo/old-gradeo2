package com.unnamed.gradeo.core.dtos.groups;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class GroupViewDto {
    private int id;
    private String name;
    private Integer instituteId;
    private Integer departmentId;
    private String instituteName;
    private String departmentName;
}
