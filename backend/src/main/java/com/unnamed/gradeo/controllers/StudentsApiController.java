package com.unnamed.gradeo.controllers;

import an.awesome.pipelinr.Pipeline;
import com.unnamed.gradeo.core.dtos.students.StudentViewDto;
import com.unnamed.gradeo.core.features.students.create.CreateStudentCommand;
import com.unnamed.gradeo.core.features.students.delete.DeleteStudentCommand;
import com.unnamed.gradeo.core.features.students.update.UpdateStudentCommand;
import com.unnamed.gradeo.core.features.students.view.GetStudentByIdQuery;
import com.unnamed.gradeo.core.features.students.view.GetStudentsQuery;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/students")
@AllArgsConstructor
public class StudentsApiController {

    private final Pipeline pipeline;

    @GetMapping
    public List<StudentViewDto> getAllStudents() {
        return pipeline.send(new GetStudentsQuery());
    }

    @GetMapping("{id}")
    public StudentViewDto getStudentById(@PathVariable Integer id) {
        return pipeline.send(new GetStudentByIdQuery(id));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createStudent(@RequestBody CreateStudentCommand command) {
        pipeline.send(command);
    }

    @PutMapping
    public void updateStudent(@RequestBody UpdateStudentCommand command) {
        pipeline.send(command);
    }

    @DeleteMapping("{id}")
    public void deleteStudentById(@PathVariable int id) {
        pipeline.send(new DeleteStudentCommand(id));
    }

}
