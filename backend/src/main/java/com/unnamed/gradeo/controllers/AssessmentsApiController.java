package com.unnamed.gradeo.controllers;

import an.awesome.pipelinr.Pipeline;
import com.unnamed.gradeo.core.dtos.assessments.AssessmentViewDto;
import com.unnamed.gradeo.core.features.assessments.create.CreateAssessmentCommand;
import com.unnamed.gradeo.core.features.assessments.delete.DeleteAssessmentCommand;
import com.unnamed.gradeo.core.features.assessments.update.UpdateAssessmentCommand;
import com.unnamed.gradeo.core.features.assessments.view.GetAssessmentByIdQuery;
import com.unnamed.gradeo.core.features.assessments.view.GetAssessmentsQuery;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/assessments")
@AllArgsConstructor
public class AssessmentsApiController {

    private final Pipeline pipeline;

    @GetMapping
    public List<AssessmentViewDto> getAllAssessments() {
        return pipeline.send(new GetAssessmentsQuery());
    }

    @GetMapping("{id}")
    public AssessmentViewDto getAssessmentById(@PathVariable Integer id) {
        return pipeline.send(new GetAssessmentByIdQuery(id));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createAssessment(@RequestBody CreateAssessmentCommand command) {
        pipeline.send(command);
    }

    @PutMapping
    public void updateAssessment(@RequestBody UpdateAssessmentCommand command) {
        pipeline.send(command);
    }

    @DeleteMapping("{id}")
    public void deleteAssessmentById(@PathVariable int id) {
        pipeline.send(new DeleteAssessmentCommand(id));
    }

}
