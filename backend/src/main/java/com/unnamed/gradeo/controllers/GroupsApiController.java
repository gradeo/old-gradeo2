package com.unnamed.gradeo.controllers;

import an.awesome.pipelinr.Pipeline;
import com.unnamed.gradeo.core.dtos.groups.GroupViewDto;
import com.unnamed.gradeo.core.features.groups.create.CreateGroupCommand;
import com.unnamed.gradeo.core.features.groups.delete.DeleteGroupCommand;
import com.unnamed.gradeo.core.features.groups.update.UpdateGroupCommand;
import com.unnamed.gradeo.core.features.groups.view.GetGroupByIdQuery;
import com.unnamed.gradeo.core.features.groups.view.GetGroupsQuery;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/groups")
@AllArgsConstructor
public class GroupsApiController {

    public final Pipeline pipeline;

    @GetMapping
    public List<GroupViewDto> getAllGroups() {
        return pipeline.send(new GetGroupsQuery());
    }

    @GetMapping("{id}")
    public GroupViewDto getGroupById(@PathVariable Integer id) {
        return pipeline.send(new GetGroupByIdQuery(id));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createGroup(@RequestBody CreateGroupCommand command) {
        pipeline.send(command);
    }

    @PutMapping
    public void updateGroup(@RequestBody UpdateGroupCommand command) {
        pipeline.send(command);
    }

    @DeleteMapping("{id}")
    public void deleteGroupById(@PathVariable Integer id) {
        pipeline.send(new DeleteGroupCommand(id));
    }
}
