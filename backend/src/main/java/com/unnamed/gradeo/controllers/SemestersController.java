package com.unnamed.gradeo.controllers;

import an.awesome.pipelinr.Pipeline;
import com.unnamed.gradeo.core.dtos.semesters.SemesterViewDto;
import com.unnamed.gradeo.core.features.semesters.update.UpdateSemesterCommand;
import com.unnamed.gradeo.core.features.semesters.view.GetAllSemestersQuery;
import com.unnamed.gradeo.core.features.semesters.view.GetSemesterByIdQuery;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/semesters")
@AllArgsConstructor
public class SemestersController {

    public final Pipeline pipeline;

    @PutMapping
    public void updateSemester(@RequestBody UpdateSemesterCommand command){
        pipeline.send(command);
    }

    @GetMapping
    public List<SemesterViewDto> getAllSemesters(){
        return pipeline.send(new GetAllSemestersQuery());
    }

    @GetMapping(value = "{id}")
    public SemesterViewDto getSemesterById(@PathVariable Integer id) {
        return pipeline.send(new GetSemesterByIdQuery(id));
    }
}
