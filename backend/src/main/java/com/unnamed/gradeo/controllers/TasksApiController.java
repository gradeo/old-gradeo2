package com.unnamed.gradeo.controllers;

import an.awesome.pipelinr.Pipeline;
import com.unnamed.gradeo.core.dtos.tasks.TaskViewDto;
import com.unnamed.gradeo.core.features.tasks.create.CreateTaskCommand;
import com.unnamed.gradeo.core.features.tasks.delete.DeleteTaskCommand;
import com.unnamed.gradeo.core.features.tasks.update.UpdateTaskCommand;
import com.unnamed.gradeo.core.features.tasks.view.GetTaskByIdQuery;
import com.unnamed.gradeo.core.features.tasks.view.GetTasksQuery;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/tasks")
@AllArgsConstructor
public class TasksApiController {

    private final Pipeline pipeline;

    @GetMapping
    public List<TaskViewDto> getAllTasks() {
        return pipeline.send(new GetTasksQuery());
    }

    @GetMapping("{id}")
    public TaskViewDto getTaskById(@PathVariable Integer id) {
        return pipeline.send(new GetTaskByIdQuery(id));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createTask(@RequestBody CreateTaskCommand command) {
        pipeline.send(command);
    }

    @PutMapping
    public void updateTask(@RequestBody UpdateTaskCommand command) {
        pipeline.send(command);
    }

    @DeleteMapping("{id}")
    public void deleteTaskById(@PathVariable int id) {
        pipeline.send(new DeleteTaskCommand(id));
    }

}
