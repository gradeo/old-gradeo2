package com.unnamed.gradeo.controllers;

import an.awesome.pipelinr.Pipeline;
import com.unnamed.gradeo.core.features.departments.importing.ImportDepartmentCommand;
import com.unnamed.gradeo.core.features.groups.importing.ImportGroupCommand;
import com.unnamed.gradeo.core.features.institutes.importing.ImportInstituteCommand;
import com.unnamed.gradeo.core.features.students.importing.ImportStudentCommand;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("api/import")
@AllArgsConstructor
public class ImportApiController {

    private final Pipeline pipeline;

    @PostMapping("institutes")
    @ResponseStatus(HttpStatus.CREATED)
    public void importInstitutes(@RequestParam("file") MultipartFile file) {
        ImportInstituteCommand command = new ImportInstituteCommand();
        command.setFile(file);
        pipeline.send(command);
    }

    @PostMapping("departments")
    @ResponseStatus(HttpStatus.CREATED)
    public void importDepartments(@RequestParam("file") MultipartFile file) {
        ImportDepartmentCommand command = new ImportDepartmentCommand();
        command.setFile(file);
        pipeline.send(command);
    }

    @PostMapping("groups")
    @ResponseStatus(HttpStatus.CREATED)
    public void importGroups(@RequestParam("file") MultipartFile file) {
        ImportGroupCommand command = new ImportGroupCommand();
        command.setFile(file);
        pipeline.send(command);
    }

    @PostMapping("students")
    @ResponseStatus(HttpStatus.CREATED)
    public void importStudents(@RequestParam("file") MultipartFile file) {
        ImportStudentCommand command = new ImportStudentCommand();
        command.setFile(file);
        pipeline.send(command);
    }


}
