package com.unnamed.gradeo.controllers;

import an.awesome.pipelinr.Pipeline;
import com.unnamed.gradeo.core.dtos.attendance.AttendanceViewDto;
import com.unnamed.gradeo.core.features.attendance.create.CreateAttendanceCommand;
import com.unnamed.gradeo.core.features.attendance.delete.DeleteAttendanceCommand;
import com.unnamed.gradeo.core.features.attendance.update.UpdateAttendanceCommand;
import com.unnamed.gradeo.core.features.attendance.view.GetAttendanceByIdQuery;
import com.unnamed.gradeo.core.features.attendance.view.GetAttendancesForLessonQuery;
import com.unnamed.gradeo.core.features.attendance.view.GetAttendancesQuery;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/attendances")
@AllArgsConstructor
public class AttendanceApiController {

    private final Pipeline pipeline;

    @GetMapping
    public List<AttendanceViewDto> getAllAttendances() {
        return pipeline.send(new GetAttendancesQuery());
    }

    @GetMapping("{id}")
    public AttendanceViewDto getAttendanceById(@PathVariable Integer id) {
        return pipeline.send(new GetAttendanceByIdQuery(id));
    }

    @GetMapping("lesson/{id}")
    public List<AttendanceViewDto> getAttendanceForLessonId(@PathVariable Integer id) {
        return pipeline.send(new GetAttendancesForLessonQuery(id));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createAttendance(@RequestBody CreateAttendanceCommand command) {
        pipeline.send(command);
    }

    @PutMapping
    public void updateAttendance(@RequestBody UpdateAttendanceCommand command) {
        pipeline.send(command);
    }

    @DeleteMapping("{id}")
    public void deleteAttendanceById(@PathVariable int id) {
        pipeline.send(new DeleteAttendanceCommand(id));
    }

}
