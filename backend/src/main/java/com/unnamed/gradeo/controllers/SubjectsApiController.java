package com.unnamed.gradeo.controllers;

import an.awesome.pipelinr.Pipeline;
import com.unnamed.gradeo.core.dtos.subjects.SubjectViewDto;
import com.unnamed.gradeo.core.features.subjects.create.CreateSubjectCommand;
import com.unnamed.gradeo.core.features.subjects.delete.DeleteSubjectCommand;
import com.unnamed.gradeo.core.features.subjects.update.UpdateSubjectCommand;
import com.unnamed.gradeo.core.features.subjects.view.GetSubjectByIdQuery;
import com.unnamed.gradeo.core.features.subjects.view.GetSubjectsQuery;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/subjects")
@AllArgsConstructor
public class SubjectsApiController {

    private final Pipeline pipeline;

    @GetMapping
    public List<SubjectViewDto> getAllSubjects() {
        return pipeline.send(new GetSubjectsQuery());
    }

    @GetMapping("{id}")
    public SubjectViewDto getSubjectById(@PathVariable Integer id) {
        return pipeline.send(new GetSubjectByIdQuery(id));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createSubject(@RequestBody CreateSubjectCommand command) {
        pipeline.send(command);
    }

    @PutMapping
    public void updateSubject(@RequestBody UpdateSubjectCommand command) {
        pipeline.send(command);
    }

    @DeleteMapping("{id}")
    public void deleteSubjectById(@PathVariable int id) {
        pipeline.send(new DeleteSubjectCommand(id));
    }

}
