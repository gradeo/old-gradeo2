package com.unnamed.gradeo.controllers;

import an.awesome.pipelinr.Pipeline;
import com.unnamed.gradeo.core.dtos.institutes.InstituteViewDto;
import com.unnamed.gradeo.core.features.institutes.create.CreateInstituteCommand;
import com.unnamed.gradeo.core.features.institutes.delete.DeleteInstituteCommand;
import com.unnamed.gradeo.core.features.institutes.update.UpdateInstituteCommand;
import com.unnamed.gradeo.core.features.institutes.view.GetInstituteByIdQuery;
import com.unnamed.gradeo.core.features.institutes.view.GetInstitutesQuery;
import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/institutes")
public class InstitutesApiController {

    private final Pipeline pipeline;

    public InstitutesApiController(@NonNull Pipeline pipeline) {
        this.pipeline = pipeline;
    }

    @GetMapping
    public List<InstituteViewDto> getAllInstitutes() {
        return pipeline.send(new GetInstitutesQuery());
    }

    @GetMapping("{id}")
    public InstituteViewDto getInstituteById(@PathVariable Integer id) {
        return pipeline.send(new GetInstituteByIdQuery(id));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createInstitute(@RequestBody CreateInstituteCommand command) {
        pipeline.send(command);
    }

    @PutMapping
    public void updateInstitute(@RequestBody UpdateInstituteCommand command) {
        pipeline.send(command);
    }

    @DeleteMapping("{id}")
    public void deleteInstituteById(@PathVariable Integer id) {
        pipeline.send(new DeleteInstituteCommand(id));
    }
}
