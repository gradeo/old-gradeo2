package com.unnamed.gradeo.controllers;

import an.awesome.pipelinr.Pipeline;
import com.unnamed.gradeo.core.dtos.departments.DepartmentViewDto;
import com.unnamed.gradeo.core.features.departments.create.CreateDepartmentCommand;
import com.unnamed.gradeo.core.features.departments.delete.DeleteDepartmentCommand;
import com.unnamed.gradeo.core.features.departments.update.UpdateDepartmentCommand;
import com.unnamed.gradeo.core.features.departments.view.GetDepartmentByIdQuery;
import com.unnamed.gradeo.core.features.departments.view.GetDepartmentsQuery;
import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/departments")
public class DepartmentsApiController {

    private final Pipeline pipeline;

    public DepartmentsApiController(@NonNull Pipeline pipeline) {
        this.pipeline = pipeline;
    }

    @GetMapping
    public List<DepartmentViewDto> getAllDepartments() {
        return pipeline.send(new GetDepartmentsQuery());
    }

    @GetMapping("{id}")
    public DepartmentViewDto getDepartmentById(@PathVariable int id) {
        return pipeline.send(new GetDepartmentByIdQuery(id));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createDepartment(@RequestBody CreateDepartmentCommand command) {
        pipeline.send(command);
    }

    @PutMapping
    public void updateDepartment(@RequestBody UpdateDepartmentCommand command) {
        pipeline.send(command);
    }

    @DeleteMapping("{id}")
    public void deleteDepartmentById(@PathVariable int id) {
        pipeline.send(new DeleteDepartmentCommand(id));
    }
}
