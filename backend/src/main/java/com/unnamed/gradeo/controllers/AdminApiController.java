package com.unnamed.gradeo.controllers;

import an.awesome.pipelinr.Pipeline;
import com.unnamed.gradeo.core.features.keycloak.users.create.CreateUserCommand;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/users")
public class AdminApiController {
    //Для работы с user-токеном
    private KeycloakRestTemplate userAccessRestTemplate;

    //Для работы с client-токеном
    private RestTemplate clientAccessRestTemplate;
    private final Pipeline pipeline;

    public AdminApiController(KeycloakRestTemplate userAccessRestTemplate,
                              RestTemplate clientAccessRestTemplate,
                              Pipeline pipeline) {
        this.userAccessRestTemplate = userAccessRestTemplate;
        this.clientAccessRestTemplate = clientAccessRestTemplate;
        this.pipeline = pipeline;
    }

    @Value("${keycloak.auth-server-url}")
    private String AUTH_SERVER_URL;

    @Value("${spring.keycloak-users-url}")
    private String USERS_URL;

    @GetMapping("whoami")
    public AccessToken handleUserInfoRequest(HttpServletRequest request) {
        KeycloakAuthenticationToken token = (KeycloakAuthenticationToken) request.getUserPrincipal();
        KeycloakPrincipal principal = (KeycloakPrincipal) token.getPrincipal();
        KeycloakSecurityContext session = principal.getKeycloakSecurityContext();
        return session.getToken();
    }

    @GetMapping("all")
    public List users() {
        return clientAccessRestTemplate.getForEntity(URI.create(AUTH_SERVER_URL + USERS_URL), List.class)
                .getBody();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void register(@RequestBody CreateUserCommand command) {
        pipeline.send(command);
    }
}
