package com.unnamed.gradeo.controllers;

import an.awesome.pipelinr.Pipeline;
import com.unnamed.gradeo.core.dtos.lessons.LessonViewDto;
import com.unnamed.gradeo.core.features.lessons.create.CreateLessonCommand;
import com.unnamed.gradeo.core.features.lessons.delete.DeleteLessonCommand;
import com.unnamed.gradeo.core.features.lessons.update.UpdateLessonCommand;
import com.unnamed.gradeo.core.features.lessons.view.GetLessonByIdQuery;
import com.unnamed.gradeo.core.features.lessons.view.GetLessonsQuery;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/lessons")
@AllArgsConstructor
public class LessonsApiController {

    private final Pipeline pipeline;

    @GetMapping
    public List<LessonViewDto> getAllLessons() {
        return pipeline.send(new GetLessonsQuery());
    }

    @GetMapping("{id}")
    public LessonViewDto getLessonById(@PathVariable int id) {
        return pipeline.send(new GetLessonByIdQuery(id));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createLesson(@RequestBody CreateLessonCommand command) {
        pipeline.send(command);
    }

    @PutMapping
    public void updateLesson(@RequestBody UpdateLessonCommand command) {
        pipeline.send(command);
    }

    @DeleteMapping("{id}")
    public void deleteLessonById(@PathVariable int id) {
        pipeline.send(new DeleteLessonCommand(id));
    }
}
