package com.unnamed.gradeo.repositories;

import com.unnamed.gradeo.core.entities.Task;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TaskRepository extends RepositoryBase<Task, Integer> {
    List<Task> findAllBySubjectIdAndRemovedDateIsNull(Integer id);

    Optional<Task> findByIdAndRemovedDateIsNull(Integer integer);
}
