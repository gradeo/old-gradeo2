package com.unnamed.gradeo.repositories;

import com.unnamed.gradeo.core.entities.Student;
import lombok.NonNull;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Репозиторий студентов.
 */
@Repository
public interface StudentsRepository extends RepositoryBase<Student, Integer> {

    Optional<Student> findByIdAndRemovedDateIsNull(Integer integer);

    Optional<Student> findByGradeBookAndRemovedDateIsNull(Integer integer);

    Optional<Student> findByNameAndRemovedDateIsNull(String name);
    @Override
    Iterable<Student> findAllByRemovedDateIsNullOrderByIdAsc();


    List<Student> findAllBySemesterIdAndRemovedDateIsNull(Integer id);

    List<Student> findAllBySemesterIdAndRemovedDateIsNullOrderByNameAsc(Integer id);

    List<Student> findAllByNameAndGradeBookAndRemovedDateIsNull(@NonNull String name, Integer gradeBook);
}

