package com.unnamed.gradeo.repositories;

import com.unnamed.gradeo.core.entities.Institute;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Репозиторий институтов.
 */
@Repository
public interface InstituteRepository extends RepositoryBase<Institute, Integer>, JpaSpecificationExecutor<Institute> {
    @Override
    @EntityGraph(attributePaths = "departments")
    Iterable<Institute> findAllByRemovedDateIsNullOrderByIdAsc();

    Optional<Institute> findByIdAndRemovedDateIsNull(Integer id);

    @EntityGraph(attributePaths = "departments")
    Optional<Institute> findByNameAndRemovedDateIsNull(String name);

    @EntityGraph(attributePaths = "departments")
    Iterable<Institute> findAllByRemovedDateIsNotNullOrderByIdAsc();
}
