package com.unnamed.gradeo.repositories;

import com.unnamed.gradeo.core.entities.Assessment;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

public interface AssessmentsRepository
        extends RepositoryBase<Assessment, Integer>,
        JpaSpecificationExecutor<Assessment> {

    Optional<Assessment> findByIdAndRemovedDateIsNull(Integer id);
    List<Assessment> findAllByTaskIdAndRemovedDateIsNull(Integer id);

    Optional<Assessment> findByTaskIdAndStudentIdAndRemovedDateIsNull(Integer taskId, Integer studentId);
}
