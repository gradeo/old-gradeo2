package com.unnamed.gradeo.repositories;

import com.unnamed.gradeo.core.entities.Semester;

import java.util.Collection;
import java.util.Optional;

public interface SemestersRepository extends RepositoryBase<Semester, Integer> {

    Optional<Semester> findByIdAndRemovedDateIsNull(Integer id);

    Iterable<Semester> findAllByRemovedDateIsNull();
}