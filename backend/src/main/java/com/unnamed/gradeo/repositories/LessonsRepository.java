package com.unnamed.gradeo.repositories;

import com.unnamed.gradeo.core.entities.Lesson;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Репозиторий занятий.
 */
@Repository
public interface LessonsRepository extends RepositoryBase<Lesson, Integer> {
    @Override
    Iterable<Lesson> findAllByRemovedDateIsNullOrderByIdAsc();

    Optional<Lesson> findByIdAndRemovedDateIsNull(Integer id);

    List<Lesson> findAllBySubjectIdAndSemesterIdAndRemovedDateIsNullAndDateBetween(
            Integer subjectId,
            Integer semesterId,
            LocalDateTime startDate,
            LocalDateTime endDate);

    List<Lesson> findAllByTeacherIdAndRemovedDateIsNullAndDateBetween(
            Integer teacherId,
            LocalDateTime startDate,
            LocalDateTime endDate);

    List<Lesson> findAllBySemesterIdAndDateAndRemovedDateIsNull(Integer semesterId, LocalDateTime date);
}