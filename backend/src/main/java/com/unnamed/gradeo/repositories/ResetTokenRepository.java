package com.unnamed.gradeo.repositories;

import com.unnamed.gradeo.core.entities.ResetToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ResetTokenRepository extends JpaRepository<ResetToken, Integer> {
    ResetToken findByToken(UUID token);
}
