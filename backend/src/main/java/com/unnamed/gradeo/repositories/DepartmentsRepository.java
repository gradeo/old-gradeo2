package com.unnamed.gradeo.repositories;

import com.unnamed.gradeo.core.entities.Department;
import lombok.NonNull;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Репозиторий кафедр.
 */
@Repository
public interface DepartmentsRepository
        extends RepositoryBase<Department, Integer>,
        JpaSpecificationExecutor<Department> {
    @Override
    Optional<Department> findById(Integer integer);

    Optional<Department> findByIdAndRemovedDateIsNull(Integer id);

    Iterable<Department> findAllByInstituteIdAndRemovedDateIsNull(Integer integer);

    Iterable<Department> findAllByNameAndRemovedDateIsNull(@NonNull String name);

    @EntityGraph(attributePaths = "institute")
    Optional<Department> findByNameAndRemovedDateIsNull(String name);
}

