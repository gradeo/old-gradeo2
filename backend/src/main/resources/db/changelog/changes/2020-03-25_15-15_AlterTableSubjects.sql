alter table subjects
    add column department_id int;

alter table subjects
    add foreign key (department_id) references departments (id);