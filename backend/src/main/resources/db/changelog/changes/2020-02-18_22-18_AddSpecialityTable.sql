create table speciality
(
    name            serial not null,
    speciality_code varchar(255),
    created_date    timestamp without time zone,
    removed_date    timestamp without time zone,
    updated_date    timestamp without time zone,
    primary key (name)
);