create table semesters
(
    id              serial      not null,
    number          int         not null,
    group_id        int         not null,
    end_date        date        null,
    start_date      date        null,
    created_date    timestamp   not null,
    updated_date    timestamp   null,
    removed_date    timestamp   null,
    primary key (id),
    foreign key (group_id) references groups (id)
);