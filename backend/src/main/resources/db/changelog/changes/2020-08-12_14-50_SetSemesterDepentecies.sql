alter table students
    add column semester_id int not null;

alter table students
    add foreign key (semester_id) references semesters (id);

alter table lessons
    add column semester_id int not null;

alter table lessons
    add foreign key (semester_id) references semesters (id);

create table semesters_subjects
(
    semester_id int not null,
    subject_id int not null,
    foreign key (semester_id) references semesters (id),
    foreign key (subject_id) references subjects (id),
    primary key (semester_id,subject_id)
);

