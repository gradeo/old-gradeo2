alter table students
    drop column group_id;

alter table lessons
    drop column group_id;

drop table groups_subjects;