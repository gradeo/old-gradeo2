create table reset_token
(
    id          serial not null,
    token       uuid,
    user_id     integer,
    expiry_date date,
    is_used     boolean,
    primary key (id)
);