alter table users
    add column teacher_id int;

alter table users
    add foreign key (teacher_id) references teacher (id);