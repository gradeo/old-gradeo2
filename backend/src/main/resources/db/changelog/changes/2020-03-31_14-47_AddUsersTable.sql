create table users
(
    id           serial       not null,
    username     varchar(255) not null,
    password     varchar(255) not null,
    email        varchar(100) not null,
    active       bool,
    created_date timestamp    null,
    updated_date timestamp    null,
    removed_date timestamp    null,
    primary key (id)
);